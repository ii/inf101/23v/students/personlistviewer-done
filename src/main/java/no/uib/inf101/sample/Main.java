package no.uib.inf101.sample;

import javax.swing.JFrame;

import no.uib.inf101.sample.controller.AddPersonController;
import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.model.PersonList;
import no.uib.inf101.sample.view.ViewMain;

/**
 * Start the application. Creates the event bus, model, view and
 * controller, and shows the view in a window.
 */
public class Main {

  /**
   * Start the application.
   *
   * @param args command line arguments (are ignored)
   */
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    PersonList model = new PersonList(eventBus);
    ViewMain view = new ViewMain(model, eventBus);
    new AddPersonController(model, eventBus);

    JFrame frame = new JFrame("INF101 Person List");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.getMainPanel());
    frame.pack();
    frame.setVisible(true);
  }
}
