package no.uib.inf101.sample.model;

/**
 * A person. Contains a name and an age.
 */
public record Person(String name, int age) { }
