package no.uib.inf101.sample.eventbus;

import java.util.ArrayList;
import java.util.List;

/**
* A simple event bus.
*/
public class EventBus {
  private final List<EventHandler> eventHandlers = new ArrayList<>();
  
  /**
  * Register an event handler.
  * @param eventHandler the event handler to register
  */
  public void register(EventHandler eventHandler) {
    this.eventHandlers.add(eventHandler);
  }
  
  /**
  * Post an event to all registered event handlers.
  * @param event the event to post
  */
  public void post(Event event) {
    for (EventHandler eventHandler : this.eventHandlers) {
      eventHandler.handle(event);
    }
  }
}
